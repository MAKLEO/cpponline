#include<iostream>
using namespace std;

//Step 1 : Define class with data member(s)
class Singleton
{
private:
	int number;
private:
	//Step 2: Define private constructor. Now only member can create object
	Singleton( int number = 0 )
	{
		this->number = number;
	}
private:
	//Step 3: Define private Copy constructor. Now only member function can initialize & create copy.
	Singleton( const Singleton &other)
	{
		this->number = other.number;
	}
public:
	//A method which hides object creation from end user is called factory method
	//Step 4 : Define factory method and return object from it.
	//Step 5 : declare factory method static
	//Step 6 : declare local object static so that only one instance will be created.
	//Step 7 : Since copy ctor is private we can not return object by value.
	static Singleton& getInstance( void )
	{
		static Singleton instance;	//Per function call, compiler will not create object.
		return instance;
	}
public:
	int getNumber( void )const
	{
		return this->number;
	}
	void setNumber( const int number )
	{
		this->number = number;
	}
};

int main( void )
{
	Singleton &s1 =  Singleton::getInstance();
	s1.setNumber(100);
	cout<<"Number	:	"<<s1.getNumber()<<endl;
	return 0;
}
