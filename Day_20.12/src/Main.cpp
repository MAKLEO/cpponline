#include<iostream>
#include<string>
using namespace std;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( string message = "Illegal Argument Exception" ) throw( ) : message( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};
class Shape
{
protected:
	float area;
public:
	Shape( void )throw( ) : area( 0 )
	{	}

	virtual void calculateArea( void )throw( ) = 0;

	float getArea( void )const throw( )
	{
		return this->area;
	}

	virtual ~Shape( ){	}
};
class Rectangle : public Shape
{
private:
	float length;
	float breadth;
public:
	Rectangle( void )throw( ) : length( 0 ), breadth( 0 )
	{	}
	void setLength( const float length )throw( IllegalArgumentException )
	{
		if( length < 0 )
			throw IllegalArgumentException("Invalid length");
		this->length = length;
	}
	void setbreadth( const float breadth )throw( IllegalArgumentException )
	{
		if( breadth < 0 )
			throw IllegalArgumentException("Invalid breadth");
		this->breadth = breadth;
	}
	void calculateArea( void )throw( )
	{
		this->area = this->length * this->breadth;
	}
};

class Math
{
public:
	static const float PI;
public:
	static float pow( float base, int index )
	{
		float result = 1;
		for( int count = 1; count <= index; ++ count )
			result = result * base;
		return result;
	}
};
const float Math::PI = 3.142f;

class Circle : public Shape
{
private:
	float radius;
public:
	Circle( void )throw( ) : radius( 0 )
	{	}
	void setRadius( const float radius )throw( IllegalArgumentException )
	{
		if( radius < 0 )
			throw IllegalArgumentException("Invalid radius");
		this->radius = radius;
	}
	void calculateArea( void )throw( )
	{
		this->area = Math::PI * Math::pow(this->radius, 2);
	}
};
void accept_record( Shape *ptr )
{
	if( dynamic_cast<Rectangle*>( ptr ) != nullptr )
	{
		Rectangle *ptrRect = dynamic_cast<Rectangle*>( ptr );
		float length;
		cout<<"Length	:	";
		cin>>length;
		ptrRect->setLength(length);

		float breadth;
		cout<<"Breadth	:	";
		cin>>breadth;
		ptrRect->setbreadth(breadth);
	}
	else
	{
		Circle *ptrCircle = dynamic_cast<Circle*>( ptr );
		float radius;
		cout<<"Radius	:	";
		cin>>radius;
		ptrCircle->setRadius(radius);
	}
}
void print_record( Shape *ptr )
{
	cout<<"Area of instance of "<<typeid( *ptr ).name()<<" is : "<<ptr->getArea()<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Rectangle"<<endl;
	cout<<"2.Circle"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			Shape *ptr = nullptr;
			switch( choice )
			{
			case 1:
				ptr = new Rectangle();	//Upcasting
				break;
			case 2:
				ptr = new Circle( );	//Upcasting
				break;
			}
			if( ptr != nullptr )
			{
				try
				{
					::accept_record( ptr );
					ptr->calculateArea();
					::print_record( ptr );
				}
				catch( IllegalArgumentException &ex )
				{
					cout<<ex.getMessage()<<endl;
				}
			}
		}
		catch( bad_alloc &ex )
		{
			cout<<ex.what()<<endl;
		}
	}
	return 0;
}
