#include<cstdio>

namespace na
{
	int num1 = 10;	//OK
	int num3 = 30;	//OK
}
namespace na
{
	int num2 = 20;	//OK
	int num3 = 30;	//error: redefinition of 'num3'
}
int main( void )
{
	printf("Num1	:	%d\n", na::num1);	//10
	printf("Num3	:	%d\n", na::num3);	//30

	printf("Num2	:	%d\n", na::num2);	//20
	return 0;
}
