#include<iostream>
#include<vector>
using namespace std;
int main( void )
{
	vector<int> v;
	v.push_back(20);
	v.push_back(40);
	v.push_back(50);

	vector<int>::iterator itr = v.begin();
	v.insert(itr, 10);	//Insert before current position //itr-> 10
	itr = itr + 2;	//Not itr => 40
	v.insert(itr, 30);	//Insert before current position

	for( int ele : v )
		cout<<ele<<endl;
	return 0;
}
int main7( void )
{
	vector<int> v;
	for( int count = 1; count <= 10; ++ count )
		v.push_back( 10 * count );

	v.clear();	//Erase All
	if( ! v.empty(  ) )
	{
		for( int ele : v )
			cout<<ele<<endl;
	}
	else
		cout<<"Vector is empty"<<endl;

	return 0;
}
int main6( void )
{
	vector<int> v;
	for( int count = 1; count <= 10; ++ count )
		v.push_back( 10 * count );

	v.erase(v.begin());	//remove first element
	//v.erase(v.begin() + ( v.size() - 1 ) );	//remove last element
	//v.pop_back();	//remove last element

	for( vector<int>::iterator itr = v.begin(); itr != v.end(); ++ itr )
	{
		if( ( *itr ) == 50 )
		{
			v.erase(itr);	//remove element in between
			break;
		}
	}

	for( int ele : v )
		cout<<ele<<endl;
	return 0;
}
int main5( void )
{
	vector<int> v;
	v.push_back(10);
	v.push_back(200);
	v.push_back(30);

	int index = 1;
	v[ index ] = 20;

	int element = v[ index ];
	cout<<"Element at index "<<index<<" is "<<element<<endl;	//20


	cout<<v.front()<<endl;	//10
	cout<<v.back()<<endl;	//30
	return 0;
}
int main4( void )
{
	vector<int> v;
	for( int count = 1; count <= 10; ++ count )
		v.push_back( 10 * count );

	try
	{
		int index = 5;
		int element = v.at( index );
		cout<<"Element at index "<<index<<" is "<<element<<endl;

		v.at( v.size()) = 11;	//Exception : out_of_range
	}
	catch( const out_of_range &ex )
	{
		cout<<ex.what()<<endl;
	}
	return 0;
}
int main3( void )
{
	vector<int> v;
	for( int count = 1; count <= 10; ++ count )
		v.push_back( 10 * count );
	for( int element : v ) //warning: range-based for loop is a C++11 extension
		cout<<element<<endl;
	return 0;
}
int main2( void )
{
	vector<int> v;
	for( int count = 1; count <= 10; ++ count )
		v.push_back( 10 * count );

	int element = 0;
	/*
	vector<int>::iterator itrStart = v.begin();
	while( itrStart != v.end())
	{
		element = *itrStart;
		cout<<element<<endl;
		++ itrStart;
	} */

	for( vector<int>::iterator itr = v.begin(); itr != v.end(); ++ itr )
	{
		element = *itr;
		cout<<element<<endl;
	}
	return 0;
}
int main1( void )
{
	vector<int> v;
	cout<<v.capacity()<<endl;	//0
	cout<<v.size()<<endl;	//0
	cout<<"Max Size	:	"<<v.max_size()<<endl;	//4611686018427387903

	v.push_back(10);
	v.push_back(20);
	v.push_back(30);
	cout<<v.capacity()<<endl;	//4
	cout<<v.size()<<endl;	//3
	cout<<"Max Size	:	"<<v.max_size()<<endl;	//4611686018427387903

	v.push_back(40);
	v.push_back(50);
	cout<<v.capacity()<<endl;	//8
	cout<<v.size()<<endl;	//5
	cout<<"Max Size	:	"<<v.max_size()<<endl;	//4611686018427387903

	v.push_back(60);
	v.push_back(70);
	v.push_back(80);
	v.push_back(90);
	cout<<v.capacity()<<endl;	//16
	cout<<v.size()<<endl;	//9
	cout<<"Max Size	:	"<<v.max_size()<<endl;	//4611686018427387903

	v.shrink_to_fit();
	cout<<v.capacity()<<endl;	//9
	cout<<v.size()<<endl;	//9
	cout<<"Max Size	:	"<<v.max_size()<<endl;	//4611686018427387903
	return 0;
}
