#include<iostream>
using namespace std;

//Complex *ptr = nullptr;	//Not OK
int main( void )
{
	class Complex	//Local Class
	{
	private:
		int real,imag;
	public:
		Complex( int real = 0, int imag = 0 ) : real( real ), imag( imag )
		{	}
		void printRecord( void )const
		{
			cout<<this->real<<"	"<<this->imag<<endl;
		}
	};
	Complex c1;
	c1.printRecord();


	Complex *ptr = new Complex( 10, 20);
	ptr->printRecord();
	delete ptr;
	return 0;
}
