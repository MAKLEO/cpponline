#include<iostream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( )
	{
		this->real = 0;
		this->imag = 0;
	}
	explicit Complex( int number )
	{
		cout<<"Complex( int number )"<<endl;
		this->real = number;
		this->imag = number;
	}
	Complex& operator=( const Complex &other )
	{
		cout<<"Complex& operator=( const Complex &other )"<<endl;
		this->real = other.real;
		this->imag = other.imag;
		return *this;
	}
	friend ostream& operator<<( ostream &cout, const Complex &other )
	{
		cout<<"Real Number	:	"<<other.real<<endl;
		cout<<"Imag Number	:	"<<other.imag<<endl;
		return cout;
	}
};
int main( void )
{
	Complex c1;
	c1 = (Complex)10;	//c1 = Complex(10);
	//c1.operator=(Complex(10))
	cout<<c1;
	return 0;
}
