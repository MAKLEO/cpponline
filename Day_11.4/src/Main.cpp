#include<iostream>
#include<string>
using namespace std;
class Array
{
private:
	int size;
	int *arr;
public:
	Array( void )
	{
		this->size = 0;
		this->arr = nullptr;
	}
	Array( int size )
	{
		this->size = size;
		this->arr = new int[ this->size ];
	}
	Array( const Array &other )
	{
		cout<<"Array( const Array &other )"<<endl;
		this->size = other.size;
		this->arr = new int[ this->size ];
		memcpy(this->arr, other.arr, sizeof( int ) * this->size );
	}
	//const Array &other = a1
	//Array *const this = a2
	Array& operator=( const Array &other )
	{
		cout<<"Array& operator=( const Array &other )"<<endl;
		this->~Array();
		this->size = other.size;
		this->arr = new int[ this->size ];
		memcpy(this->arr, other.arr, sizeof( int ) * this->size );
		return *this;
	}
	~Array( void )
	{
		if( this->arr != nullptr )
		{
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
	friend istream& operator>>( istream& cin, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>other.arr[ index ];
		}
		return cin;
	}
	friend ostream& operator<<( ostream& cout, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
			cout<<other.arr[ index ]<<endl;
		return cout;
	}
};
int main( void )
{
	Array a1(3),a2(2);
	cin>>a1;
	a2 = a1;	//a2.operator=( a1 )
	cout<<a2;
	return 0;
}
