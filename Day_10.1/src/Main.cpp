#include<iostream>
#include<string>
using namespace std;
namespace collection
{
	class StackOverflowException
	{
	private:
		string message;
	public:
		StackOverflowException( const string message = "StackOverflowException" ) throw( ) : message( message )
		{	}
		string getMessage( void )const throw( )
		{
			return this->message;
		}
	};
	class StackUnderflowException
	{
	private:
		string message;
	public:
		StackUnderflowException( const string message = "StackUnderflowException" ) throw( ) : message( message )
		{	}
		string getMessage( void )const throw( )
		{
			return this->message;
		}
	};
	class Stack
	{
		int top;
		int size;
		int *arr;
	public:
		Stack( void )throw( bad_alloc ) : top( -1 ), size( 0 ), arr( nullptr )
		{	}
		Stack( int size )throw( bad_alloc ) : top( -1 ), size( size ), arr( new int[ this->size ] )
		{	}
		bool empty( void )const throw( )
		{
			return this->top == -1;
		}
		bool full( void )const throw( )
		{
			return this->top == this->size - 1;
		}
		void push( int element )throw( StackOverflowException )
		{
			if( this->full( ) )
				throw StackOverflowException("Stack is full");
			++ this->top;
			this->arr[ this->top ] = element;
		}
		int peek( void )const throw( StackUnderflowException )
		{
			if( this->empty( ) )
				throw StackUnderflowException("Stack is empty");
			return this->arr[ this->top ];
		}
		void pop( void )throw( StackUnderflowException )
		{
			if( this->empty( ) )
				throw StackUnderflowException("Stack is empty");
			-- this->top;
		}
		~Stack( void )
		{
			if( this->arr != nullptr )
			{
				delete[] this->arr;
				this->arr = nullptr;
			}
		}
	};
}
void accept_record( int &element )
{
	cout<<"Enter element	:	";
	cin>>element;
}
void print_record( const int &element )
{
	cout<<"Popped element is	:	"<<element<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Push"<<endl;
	cout<<"2.Pop"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
//push -> enqueue
//pop  -> dequeue
//peek ->	peek

//Deque "Deck"
int main( void )
{
	try
	{
		int choice, element;
		using namespace collection;
		Stack stk;
		while( ( choice = ::menu_list( ) ) != 0 )
		{
			try
			{
				switch( choice )
				{
				case 1:
					::accept_record(element);
					stk.push(element);
					break;
				case 2:
					element = stk.peek();
					::print_record(element);
					stk.pop();
					break;
				}
			}
			catch( StackOverflowException &ex )
			{
				cout<<ex.getMessage()<<endl;
			}
			catch( StackUnderflowException &ex )
			{
				cout<<ex.getMessage()<<endl;
			}
		}
	}
	catch( bad_alloc &ex )
	{
		cout<<"Bad memory allocation"<<endl;
	}
	return 0;
}
