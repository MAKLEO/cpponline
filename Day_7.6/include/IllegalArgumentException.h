#ifndef ILLEGALARGUMENTEXCEPTION_H_
#define ILLEGALARGUMENTEXCEPTION_H_

#include<string>
using namespace std;

namespace cpponline
{
	class IllegalArgumentException
	{
	private:
		string message;
	public:
		IllegalArgumentException( string message = "IllegalArgumentException" ) throw( );

		string getMessage( void )const throw( );
	};
}

#endif /* ILLEGALARGUMENTEXCEPTION_H_ */
