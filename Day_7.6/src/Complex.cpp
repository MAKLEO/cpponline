
#include"../include/Complex.h"
using namespace cpponline;
Complex::Complex( int real, int imag )throw( IllegalArgumentException)
{
	this->setReal(real);
	this->setImag(imag);
}
//const Complex *const this = &complex;
int Complex::getReal( void )const throw( )
{
	return this->real;
}
//Complex *const this = &complex;
void Complex::setReal( const int real )throw ( IllegalArgumentException )
{
	if( real >= 0 )
		this->real = real;
	else
		throw IllegalArgumentException("Invalid real number");
}
//const Complex *const this = &complex;
int Complex::getImag( void )const throw( )
{
	return this->imag;
}
//Complex *const this = &complex;
void Complex::setImag( const int imag )throw ( IllegalArgumentException )
{
	if( imag >= 0 )
		this->imag = imag;
	else
		throw IllegalArgumentException("Invalid imag number");
}
