#include<iostream>
using namespace std;

class LinkedList;
class Iterator;
class Node
{
private:
	int data;
	Node *next;
public:
	Node( int data = 0 )
	{
		this->data = data;
		this->next = nullptr;
	}
	friend class LinkedList;
	friend class Iterator;
};
class Iterator
{
private:
	Node *trav;
public:
	Iterator( Node *head )
	{
		this->trav = head;
	}
	bool operator!=( Iterator &other )
	{
		return this->trav != other.trav;
	}
	int operator*( void )
	{
		return trav->data;
	}
	void operator++( void )
	{
		trav = trav->next;
	}
};
class LinkedList
{
private:
	Node *head;
	Node *tail;
public:
	LinkedList( void )
	{
		this->head = nullptr;
		this->tail = nullptr;
	}
	bool empty( void )const
	{
		return this->head == nullptr;
	}
	void addLast( int data )
	{
		Node *newNode = new Node( data );
		if( this->empty( ) )
			this->head = newNode;
		else
			this->tail->next = newNode;
		this->tail = newNode;
	}
	Iterator begin( void )
	{
		Iterator itr( this->head );
		return itr;
	}
	Iterator end( void )
	{
		Iterator itr( nullptr );
		return itr;
	}
};
int main( void )
{
	LinkedList list;
	list.addLast(10);
	list.addLast(20);
	list.addLast(30);

	Iterator itrStart = list.begin();
	Iterator itrEnd = list.end();
	while( itrStart != itrEnd )	//itrStart.operator!=(itrEnd);
	{
		cout<< (*itrStart)<<"	";	//itrStart.operator*( );
		++ itrStart;	//itrStart.operator++( );
	}
	cout<<endl;
	return 0;
}
