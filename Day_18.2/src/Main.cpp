#include<iostream>
#include<string>
using namespace std;
class Book
{
private:
	string title;
	float price;
	int pageCount;
public:
	void acceptRecord( void )
	{
		cout<<"Title	:	";
		cin>>this->title;
		cout<<"Price	:	";
		cin>>this->price;
		cout<<"Page Count	:	";
		cin>>this->pageCount;
	}
	void printRecord( void )const
	{
		cout<<"Title		:	"<<this->title<<endl;
		cout<<"Price		:	"<<this->price<<endl;
		cout<<"Page Count	:	"<<this->pageCount<<endl;
	}
};
class Tape
{
private:
	string title;
	float price;
	int playTime;
public:
	void acceptRecord( void )
	{
		cout<<"Title	:	";
		cin>>this->title;
		cout<<"Price	:	";
		cin>>this->price;
		cout<<"Play Time	:	";
		cin>>this->playTime;
	}
	void printRecord( void )const
	{
		cout<<"Title		:	"<<this->title<<endl;
		cout<<"Price		:	"<<this->price<<endl;
		cout<<"Play Time	:	"<<this->playTime<<endl;
	}
};
int main( void )
{
	Book book;
	book.acceptRecord();
	book.printRecord();

	Tape tape;
	tape.acceptRecord();
	tape.printRecord();
	return 0;
}
