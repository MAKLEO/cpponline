#include<iostream>
using namespace std;

#define size	3
class Array
{
private:
	int arr[ size ];
public:
	void acceptRecord( void )
	{
		for( int index = 0; index < size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>arr[ index ];
		}
	}
	void printRecord( void )
	{
		for( int index = 0; index < size; ++ index )
			cout<<arr[ index ]<<endl;
	}
};
int main( void )
{
	Array a1;
	a1.acceptRecord( );
	a1.printRecord( );
	return 0;
}
