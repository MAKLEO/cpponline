#include<iostream>
#include<string>
using namespace std;

class Person
{
protected:
	string name;
	int age;
public:
	Person( void ) : name(""), age(0)
	{
		cout<<"Person( void )"<<endl;
	}
	Person( string name, int age ) : name( name ), age( age )
	{
		cout<<"Person( string name, int age )"<<endl;
	}
	void showRecord( void )
	{
		cout<<"Name	:	"<<this->name<<endl;
		cout<<"Age	:	"<<this->age<<endl;
	}
	~Person( void )
	{
		cout<<"~Person( void )"<<endl;
	}
};
class Employee : public Person	//Employee is a Person
{
private:
	int empid;
	float salary;
public:
	Employee( void ) : Person(), empid( 0 ), salary( 0 )
	{
		cout<<"Employee( void )"<<endl;
	}
	Employee( string name, int age, int empid, float salary ) : Person(name, age), empid( empid ), salary( salary )
	{
		cout<<"Employee( string name, int age, int empid, float salary )"<<endl;
	}
	//Employee *const this = &emp
	void displayRecord( void )
	{
		this->showRecord( );
		cout<<"Empid	:	"<<this->empid<<endl;
		cout<<"Salary	:	"<<this->salary<<endl;
	}
	~Employee( void )
	{
		cout<<"~Employee( void )"<<endl;
	}
};
int main( void )
{
	//Employee emp("Sandeep",36,33,45000.50f);
	//emp.displayRecord();

	Employee *ptr = new Employee("Sandeep",36,33,45000.50f);
	ptr->displayRecord();
	delete ptr;
	return 0;
}
int main5( void )
{
	//Person p("Sandeep", 36);
	//p.showRecord();

	Person *p = new Person("Sandeep", 36);
	p->showRecord();
	delete p;
	return 0;
}
int main4( void )
{
	Employee *ptr = new Employee( );
	cout<<endl;
	delete ptr;
	return 0;
}
int main3( void )
{
	Employee emp;
	cout<<endl;
	return 0;
}
int main2( void )
{
	Person *ptr = new Person( );
	cout<<endl;
	delete ptr;
	return 0;
}
int main1( void )
{
	Person p;
	cout<<endl;
	return 0;
}
