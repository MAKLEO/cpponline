#include<iostream>
using namespace std;

class Complex
{
private:
	int real;
	int imag;
public:
	/*Complex(  )	//Constructor
	{
		cout<<"Complex(  )"<<endl;
		this->real = 0;
		this->imag = 0;
	}*/
	Complex(  int real, int imag )	//Constructor
	{
		cout<<"Complex(  int real, int imag )"<<endl;
		(*this).real = real;
		this->imag = imag;
	}
	void acceptRecord( void )
	{
		cout<<"Real Number	:	";
		cin>>this->real;
		cout<<"Imag Number	:	";
		cin>>this->imag;
	}
	void printRecord( void )
	{
		cout<<"Real Number	:	"<<this->real<<endl;
		cout<<"Imag Number	:	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex c1(10,20);
	Complex c2;
	return 0;
}
int main1( void )
{
	//Complex c1;
	//c1.Complex();	//Not OK

	//Complex *ptr = &c1;
	//ptr->Complex( );	//Not OK

	//Complex &c2 = c1;
	//c2.Complex();	//Not OK
	return 0;
}
