#include<iostream>
using namespace std;
namespace ntest
{
	class Test
	{
	private:
		int number;
	public:
		Test( void )
		{
			this->number = 10;
		}
		friend void print( void );
	};
}
void print( void )
{
	ntest::Test t;
	cout<<"Number	:	"<<t.number<<endl;	//Not OK
}
int main( void )
{
	print( );
	return 0;
}
