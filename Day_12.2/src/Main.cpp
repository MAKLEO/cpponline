#include<cstdlib>
#include<iostream>
using namespace std;
class Array
{
private:
	int size;
	int *arr;
public:
	Array( void )
	{
		this->size = 0;
		this->arr = nullptr;
	}
	Array( int size )
	{
		this->size = size;
		this->arr = new int[ this->size ];
	}
	void acceptRecord( void )
	{
		for( int index = 0; index < this->size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>this->arr[ index ];
		}
	}
	void printRecord( void )const
	{
		for( int index = 0; index < this->size; ++ index )
			cout<<this->arr[ index ]<<endl;
	}
	~Array( void )
	{
		if( this->arr != nullptr )
		{
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
};
class AutoPtr
{
private:
	Array *ptr;
public:
	AutoPtr( Array *ptr )
	{
		this->ptr = ptr;
	}
	Array* operator->( )
	{
		return this->ptr;
	}
	~AutoPtr( void )
	{
		delete this->ptr;
		this->ptr = nullptr;
	}
};
int main( void )
{
	auto_ptr<Array> obj( new Array( 5 ) );
	obj->acceptRecord( );
	obj->printRecord( );
}
int main3( void )
{
	AutoPtr obj( new Array( 3 ) );
	obj->acceptRecord( );	//obj.operator ->()->acceptRecord();
	obj->printRecord( );	//obj.operator ->()->printRecord();
	//Here obj is a smart pointer
	return 0;
}
int main2( void )
{
	Array *ptr = new Array( 3 );
	ptr->acceptRecord();
	ptr->printRecord();
	delete ptr;
	return 0;
}
int main1( void )
{
	Array a1(3);
	a1.acceptRecord();
	a1.printRecord();
	return 0;
}
