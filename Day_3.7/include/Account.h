#ifndef ACCOUNT_H_
#define ACCOUNT_H_

namespace naccount
{
	class Account
	{
	private:
		char name[ 30 ];
		int number;
		float balance;
	public:
		void acceptRecord( void );
		void printRecord( void );
	};//end of class
}//end of namespace

#endif /* ACCOUNT_H_ */
