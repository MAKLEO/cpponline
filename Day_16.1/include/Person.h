#ifndef PERSON_H_
#define PERSON_H_

#include<iostream>
using namespace std;

#include"../include/Date.h"
#include"../include/Address.h"
class Person
{
private:
	string name;
	Date birthDate;
	Address currentAddress;
public:
	Person( void );
	Person( string name, Date birthDate, Address currentAddress );
	Person( string name, int day, int month, int year, string city, string state, int pincode );
	string getName( void );
	void setName( string name );
	Date& getBirthDate( void );
	void setBirthDate( Date birthDate );
	Address& getCurrentAddress( void );
	void setCurrentAddress( Address currentAddress );
	friend istream& operator>>( istream &cin, Person &other );
	friend ostream& operator<<( ostream &cout, const Person &other );
};
#endif /* PERSON_H_ */
