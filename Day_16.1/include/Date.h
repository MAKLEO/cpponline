#ifndef DATE_H_
#define DATE_H_

#include<iostream>
using namespace std;

class Date
{
private:
	int day;
	int month;
	int year;
public:
	Date( void );
	Date( int day, int month, int year );
	int getDay( void )const;
	void setDay( const int day );
	int getMonth( void )const;
	void setMonth( const int month );
	int getYear( void )const;
	void setYear( const int year );
	friend istream& operator>>( istream &cin, Date &other );
	friend ostream& operator<<( ostream &cout, const Date &other );
};
#endif /* DATE_H_ */
