#include<iostream>
using namespace std;
class Base
{
private:
	int num1;
	int num2;
public:
	Base( void )
	{
		this->num1 = 10;
		this->num2 = 20;
	}
	Base( int num1, int num2 )
	{
		this->num1 = num1;
		this->num2 = num2;
	}
	void showRecord( void )const
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
	void printRecord( void )const
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
	}
};
class Derived : public Base
{
private:
	int num3;
public:
	Derived( void )
	{
		this->num3 = 30;
	}
	Derived( int num1, int num2, int num3 ) : Base(num1, num2)
	{
		this->num3 = num3;
	}
	void printRecord( void )const
	{
		Base::printRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
	void displayRecord( void )const
	{
		Base::showRecord();
		cout<<"Num3	:	"<<this->num3<<endl;
	}
};

int main( void )
{
	//Base * ptrBase = nullptr;	//OK
	//Base * ptrBase = new Base();	//OK
	//Base * ptrBase = new Derived( );	//OK

	//Derived *ptrDerived = nullptr;	//OK
	//Derived *ptrDerived = new Derived();	//OK
	//Derived *ptrDerived = new Base();	//Not OK
	return 0;
}
int main15( void )
{
	//Derived *ptr = nullptr;	//OK
	//Derived *ptr = new Derived();	//OK
	//Derived *ptr = new Base( );	//Not OK
	return 0;
}
int main14( void )
{
	Base base(50,60);
	Derived derived;
	//derived = base;	//Not OK
	derived.printRecord();
	return 0;
}
int main13( void )
{
	Derived d1;
	Base &b1 = d1;	//Upcasting
	b1.printRecord();

	Derived &d2 = ( Derived&)b1;	//Downcasting
	d2.printRecord();

	return 0;
}
int main12( void )
{
	Base *ptrBase = new Derived( );	//Upcasting
	ptrBase->printRecord();
	cout<<endl;
	Derived *ptrDerived = (Derived*)ptrBase;//Downcasting
	ptrDerived->printRecord();
	delete ptrBase;
	return 0;
}
int main11( void )
{
	Derived derived;
	Base &base = derived;	//Upcasting
	//Base *const base = &derived;	//Upcasting
	base.printRecord();
	return 0;
}
int main10( void )
{
	Base *ptrBase = new Derived( );	//OK : UpCasting
	ptrBase->printRecord();
	delete ptrBase;
	return 0;
}
int main9( void )
{
	Derived *ptrDerived = new Derived( );
	//Base *ptrBase = (Base*)ptrDerived;	//UpCasting
	Base *ptrBase = ptrDerived;	//UpCasting
	ptrBase->printRecord();
	delete ptrBase;
	return 0;
}
int main8( void )
{
	Base base;
	Derived derived;
	//Base *ptr = nullptr;	//OK
	//Base *ptr = &base;	//OK
	//Base *ptr = &derived;	//OK
	return 0;
}
int main7( void )
{
	Base base;
	Derived derived(50,60,70);
	base = derived;	//Object Slicing
	base.printRecord();	//Base::printRecord( );
	return 0;
}
int main6( void )
{
	Derived d1(50,60,70);
	Derived d2;
	d2 = d1;	//d2.operator=(d1);
	d2.printRecord();	//Derived::printRecord( );
	return 0;
}
int main5( void )
{
	Base b1( 50,60);
	Base b2 = b1;	//Here on b2, copy ctor will call
	b2.printRecord();	//Base::printRecord( );
	return 0;
}
int main4( void )
{
	Derived *ptrDerived = new Derived( );	//Base() -> Derived( )
	//ptrDerived->showRecord( );	//Base::showRecord( )
	//ptrDerived->printRecord( );	//Derived::printRecord( )
	//ptrDerived->Base::printRecord( );	//Base::printRecord( )
	//ptrDerived->displayRecord( );	//Derived::displayRecord( );
	delete ptrDerived;	//~Derived() -> ~Base( )
	return 0;
}
int main3( void )
{
	Derived derived;
	//derived.showRecord( );	//Base::showRecord( )
	//derived.printRecord( );	//Derived::printRecord( )
	//derived.Base::printRecord( );	//Base::printRecord( )
	//derived.displayRecord( );	//Derived::displayRecord( );
	return 0;
}
int main2( void )
{
	Base *ptrBase = new Base( );	////Base::Base( );
	//ptrBase->showRecord( );	//Base::showRecord( )
	//ptrBase->printRecord( );	//Base::printRecord( );
	//ptrBase->Derived::printRecord( );	//Compiler Error
	//ptrBase->displayRecord( );	//Compiler Error
	delete ptrBase;	//Base::~Base( );
	return 0;
}
int main1( void )
{
	Base base;
	//base.showRecord( );	//Base::showRecord( )
	//base.printRecord( );	//Base::printRecord( );
	//base.Derived::printRecord( );	//Compiler Error
	//base.displayRecord( );	//Compiler Error
	return 0;
}
