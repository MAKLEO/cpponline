#include<iostream>
using namespace std;

class InstanceCounter
{
private:
	static int count;
public:
	InstanceCounter( void )
	{
		++ InstanceCounter::count;
	}
	static int getCount( void )
	{
		return InstanceCounter::count;
	}
};
int InstanceCounter::count = 0;
int main( void )
{
	InstanceCounter ic1, ic2, ic3;
	cout<<"Instance Counter	:	"<<InstanceCounter::getCount()<<endl;
	return 0;
}
