#include<iostream>
#include<string>
using namespace std;

template<class K, class V >
class Pair
{
private:
	K key;
	V value;
public:
	Pair( K key = K(), V value = V() )
	{
		this->key = key;
		this->value = value;
	}
	K getKey( void )const
	{
		return this->key;
	}
	void setKey( K key )
	{
		this->key = key;
	}
	V getValue( void )const
	{
		return this->value;
	}
	void setValue( V value )
	{
		this->value = value;
	}
};
int main( void )
{
	Pair<int, string> p;	//<int, string> <= template argument list
	p.setKey(1998);
	p.setValue("CDAC");
	cout<<"Key	:	"<<p.getKey()<<endl;
	cout<<"Value	:	"<<p.getValue()<<endl;
	return 0;
}
