#include<iostream>
using namespace std;


class LinkedList
{
private:
	//Encapsulation
	class Node	//Nested class
	{
	private:
		int data;
		Node *next;
	public:
		Node( int data )
		{
			this->data = data;
			this->next = nullptr;
		}
		friend class LinkedList;
	};
private:
	Node *head;
	Node *tail;
public:
	LinkedList( void )
	{
		this->head = nullptr;
		this->tail = nullptr;
	}
	bool empty( void )const
	{
		return this->head == nullptr;
	}
	void addLast( int data )
	{
		Node *newNode = new Node( data );
		if( this->empty())
			this->head = newNode;
		else
			this->tail->next = newNode;
		this->tail = newNode;
	}
	void printList( void )const
	{
		if( !this->empty())
		{
			Node *trav = this->head;
			while( trav != nullptr )
			{
				cout<<trav->data<<"	";
				trav = trav->next;
			}
			cout<<endl;
		}
	}
};
int main( void )
{
	LinkedList list;
	list.addLast(10);
	list.addLast(20);
	list.addLast(30);
	list.printList();

	return 0;
}
