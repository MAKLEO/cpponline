#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( void )
	{
		this->xPos = 0;
		this->yPos = 0;
	}
	Point( int xPos, int yPos )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}
	friend istream& operator>>( istream &cin, Point &other )
	{
		cout<<"Enter x position	:	";
		cin>>other.xPos;
		cout<<"Enter y position	:	";
		cin>>other.yPos;
		return cin;
	}
	friend ostream& operator<<( ostream &cout, Point &other )
	{
		cout<<"X Position	:	"<<other.xPos<<endl;
		cout<<"Y Position	:	"<<other.yPos<<endl;
		return cout;
	}
};
int main( void )
{
	Point pt1, pt2;
	cin>>pt1>>pt2;	//operator>>( operator>>( cin, pt1 ), pt2 );
	cout<<pt1<<pt2;	//operator<<( operator<<( cout, pt1 ), pt2 );
	return 0;
}
int main3( void )
{
	Point pt1;
	cin>>pt1;	//operator>>( cin, pt1 );
	cout<<pt1;	//operator<<( cout, pt1 );
	return 0;
}
int main2( void )
{
	Point pt1(10,20);
	Point pt2(30,40);
	cout<<pt1<<pt2;	//operator<<( operator<<( cout, pt1 ), pt2 );
	return 0;
}
int main1( void )
{
	Point pt1(10,20);
	cout<<pt1;	//operator<<( cout, pt1 );
	return 0;
}
