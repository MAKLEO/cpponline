#include<cstring>	//C
#include<iostream>
#include<string>	//C++
using namespace std;

class StringIndexOutOfBoundsException
{
private:
	string message;
public:
	StringIndexOutOfBoundsException( string message = "StringIndexOutOfBoundsException" ) throw( ) : message( message )
	{	}
	string getMessage( void ) const throw( )
	{
		return this->message;
	}
};
class String
{
private:
	size_t length;
	char *buffer;
public:
	String( void )throw( ) : length( 0 ), buffer( nullptr )
	{	}
	String( const char *str )throw( bad_alloc )
	{
		this->length = strlen( str );
		this->buffer = new char[ this->length + 1 ];
		strcpy(this->buffer, str);
	}
	String( const String &other )
	{
		this->length = other.length;
		this->buffer = new char[ this->length + 1 ];
		strcpy(this->buffer, other.buffer);
	}
	String& operator=( const String &other )
	{
		this->~String();
		this->length = other.length;
		this->buffer = new char[ this->length + 1 ];
		strcpy(this->buffer, other.buffer);
		return *this;
	}
	char& operator[]( const int index )const throw( StringIndexOutOfBoundsException )
	{
		if( index >= 0 && index <= this->length)
			return this->buffer[ index ];
		throw StringIndexOutOfBoundsException("Invalid index");
	}
	void operator()( const char *str )throw( bad_alloc )
	{
		this->~String();
		this->length = strlen( str );
		this->buffer = new char[ this->length + 1 ];
		strcpy(this->buffer, str);
	}
	~String( void )
	{
		if( this->buffer != nullptr )
		{
			delete[] this->buffer;
			this->buffer = nullptr;
		}
	}
	friend ostream& operator<<( ostream &cout, const String &other )
	{
		if( other.buffer != nullptr )
			cout<<other.buffer<<endl;
		else
			cout<<"Empty string"<<endl;
		return cout;
	}
};

int main( void  )
{
	String s1("abc");
	s1("SunBeam");	//s1.operator()("SunBeam");		//s1 -> Function Object or Functor
	cout<<s1<<endl;
	return 0;
}

int main2( void  )
{
	String s1("SunBeam");
	char ch = s1[ 3 ];	//char ch = s1.operator[]( 3 );
	cout<<ch<<endl;
	return 0;
}

int main1( void  )
{
	String s1;
	String s2( "SunBeam");
	String s3 = s2;
	s1 = s2;

	cout<<s2<<endl;	//operator<<( cout, s2 )<<endl;
	cout<<s3<<endl; //operator<<( cout, s3 )<<endl;
	return 0;
}
