#include<iostream>
using namespace std;

class Test
{
private:
	char &ch3;
public:
	Test( char &ch2 ) : ch3( ch2 )
	{	}
};
int main( void )
{
	char ch1 = 'A';
	Test t(ch1);
	size_t size = sizeof( t );
	cout<<"Size	:	"<<size<<endl;
	return 0;
}
int main8( void )
{
	//int &num2;	//Not OK
	//int &num2 = NULL;	//Not  OK
	//int &num2 = 10;	//Not OK

	int num1 = 10;	//Variable / object
	int &num2 = num1;	//OK
	//int *const num2 = &num1;	//OK
	cout<<"Num2	:	"<<num2<<endl;
	//cout<<"Num2	:	"<<*num2<<endl;
	return 0;
}
int main7( void )
{
	int num1 = 10;
	int &num2 = num1;
	int &num3 = num2;

	++ num1;
	++ num2;
	++ num3;

	cout<<"Num1	:	"<<num1<<endl;	//OK
	cout<<"Num2	:	"<<num2<<endl;	//OK
	cout<<"Num3	:	"<<num3<<endl;	//OK
	return 0;
}
int main6( void )
{
	int num1 = 10;
	int num2 = 20;
	int &num3 = num1;
	num3 = num2;
	++ num3;

	cout<<"Num1	:	"<<num1<<endl;	//OK
	cout<<"Num2	:	"<<num2<<endl;	//OK
	cout<<"Num3	:	"<<num3<<endl;	//OK
	return 0;
}
int main5( void )
{
	int num1 = 10;		//Referent Variable
	int &num2 = num1;	//Reference Variable
	const int &num3 = num1;	//Reference Variable

	++ num1;	//OK
	++ num2;	//OK
	//++ num3;	//Not OK

	cout<<"Num1	:	"<<num1<<endl;	//OK
	cout<<"Num2	:	"<<num2<<endl;	//OK
	cout<<"Num3	:	"<<num3<<endl;	//OK
	return 0;
}

int main4( void )
{
	int num1 = 10;		//Referent Variable
	int &num2 = num1;	//Reference Variable

	++ num1;
	++ num2;

	cout<<"Num1	:	"<<num1<<endl;
	cout<<"Num2	:	"<<num2<<endl;
	return 0;
}


int main3( void )
{
	int num1 = 10;		//Referent Variable
	int &num2 = num1;	//Reference Variable

	cout<<num1<<"	"<<&num1<<endl;
	cout<<num2<<"	"<<&num2<<endl;
	return 0;
}


int main2( void )
{
	int number = 10;
	int *ptr = &number;
	cout<<number<<"	"<<&number<<endl;
	cout<<*ptr<<"	"<<ptr<<endl;
	return 0;
}

int main1( void )
{
	int num1 = 10;
	int num2 = num1;
	cout<<num1<<"	"<<&num1<<endl;
	cout<<num2<<"	"<<&num2<<endl;
	return 0;
}

