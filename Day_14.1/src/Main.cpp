#include<iostream>
#include<string>
using namespace std;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( string message = "IllegalArgumentException") throw( ) : message( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};

//Parameterized Type == Template Class
//template<typename T>	//T -> Type Parameter
template<class T>
class Array
{
private:
	int size;
	T *arr;
public:
	Array( int size = 0 ) throw( IllegalArgumentException )
	{
		this->size = size;
		if( this->size == 0 )
			this->arr = nullptr;
		else if( this->size > 0 )
			this->arr = new T[ this->size ];
		else
			throw IllegalArgumentException("Invalid Size");
	}
	void acecptRecord( void )
	{
		for( int index = 0; index < this->size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>this->arr[ index ];
		}
	}
	void resize( int size )throw( IllegalArgumentException )
	{
		if( size <= 0 )
			throw IllegalArgumentException("Invalid Size");
		if( size > this->size || size < this->size )
		{
			T *arr = new T[ size ];
			for( int index = 0; index < this->size; ++ index )
				arr[ index ] = this->arr[ index ];
			this->size = size;
			this->~Array();
			this->arr = arr;
		}
	}
	void printRecord( void )
	{
		for( int index = 0; index < this->size; ++ index )
			cout<<this->arr[ index ]<<endl;
	}
	~Array( void )
	{
		if( this->arr != nullptr )
		{
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
};
int main( void )
{
	Array<string> a1(2);	//string -> Type Argument
	a1.acecptRecord();
	a1.printRecord();


	Array<int> a2(2);	//int -> Type Argument
	a2.acecptRecord();
	a2.printRecord();

	Array<double> a3(2);	//double -> Type Argument
	a3.acecptRecord();
	a3.printRecord();
	return 0;
}
