#include<cstring>
#include<iostream>
using namespace std;
class String
{
private:
	class StringRep
	{
	private:
		int referenceCount;
		int length;
		char *buffer;
	public:
		StringRep( void )
		{
			this->referenceCount = 1;
			this->length = 0;
			this->buffer = nullptr;
		}
		StringRep( const char *str )
		{
			this->referenceCount = 1;
			this->length = strlen( str );
			this->buffer = new char[ this->length + 1 ];
			strcpy( this->buffer, str );
		}
		~StringRep( void )
		{
			if( this->buffer != nullptr )
			{
				delete[] this->buffer;
				this->buffer = nullptr;
			}
		}
		friend ostream& operator<<( ostream &cout, StringRep &other )
		{
			cout<<other.buffer<<endl;
			return cout;
		}
		friend class String;
	};
private:
	StringRep *ptr;
public:
	String( void )
	{
		this->ptr = new StringRep();
	}
	String( const char *str )
	{
		this->ptr = new StringRep( str );
	}
	String( const String &other )
	{
		++ other.ptr->referenceCount;
		this->ptr = other.ptr;
	}
	String& operator=( const String &other )
	{
		this->~String( );
		++ other.ptr->referenceCount;
		this->ptr = other.ptr;
		return *this;
	}
	void toLower( void )
	{
		-- this->ptr->referenceCount;
		this->ptr = new StringRep( this->ptr->buffer );
		int index = 0;
		while( this->ptr->buffer[ index ] != '\0' )
		{
			if( this->ptr->buffer[ index ] < 97 )
				this->ptr->buffer[ index ] = this->ptr->buffer[ index ] + 32;
			++ index;
		}
	}
	~String( void )
	{
		-- this->ptr->referenceCount;
		if( this->ptr->referenceCount == 0 )
		{
			delete this->ptr;
			this->ptr = nullptr;
		}
	}
	friend ostream& operator<<( ostream &cout, String &other )
	{
		cout<<(*other.ptr)<<endl;
		return cout;
	}
};
int main( void )
{
	String s1("SunBeam");
	String s2 = s1;
	s2.toLower();
	cout<<s1<<endl;
	cout<<s2<<endl;
	return 0;
}
