#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( void )
	{
		this->xPos = 0;
		this->yPos = 0;
	}
	Point( int xPos, int yPos )
	{
		this->xPos = xPos;
		this->yPos = yPos;
	}
	//Point *const this = &pt1
	//Point &other = pt2
	Point operator+( Point &other )
	{
		Point temp;
		temp.xPos = this->xPos + other.xPos;
		temp.yPos = this->yPos + other.yPos;
		return temp;
	}
	//Point *const this = &pt1
	//Point &other = pt2
	Point operator-( Point &other )
	{
		Point temp;
		temp.xPos = this->xPos - other.xPos;
		temp.yPos = this->yPos - other.yPos;
		return temp;
	}
	//Point *const this = &pt1
	//int value = 5;
	Point operator+( int value )
	{
		Point temp;
		temp.xPos = this->xPos + value;
		temp.yPos = this->yPos + value;
		return temp;
	}
	//Point *const this = &pt1
	//Point &other = pt2
	bool operator==( Point &other )
	{
		if( this->xPos == other.xPos && this->yPos == other.yPos )
			return true;
		return false;
	}
	//Point *const this = &pt1
	//Point &other = pt2
	void operator+=( Point &other )
	{
		this->xPos += other.xPos;
		this->yPos += other.yPos;
	}
	Point operator++( void )	//Pre-Increment
	{
		Point temp;
		temp.xPos = ++ this->xPos;
		temp.yPos = ++ this->yPos;
		return temp;
	}
	Point operator++( int )	//Post-Increment
	{
		Point temp;
		temp.xPos = this->xPos ++;
		temp.yPos = this->yPos ++;
		return temp;
	}
	void printRecord( void )
	{
		cout<<"X Position	:	"<<this->xPos<<endl;
		cout<<"Y Position	:	"<<this->yPos<<endl;
	}
};

int main( void )
{
	Point pt1(10,20);
	Point pt2;
	pt2 = pt1 ++;	//pt2 = pt1.operator++( 0 )

	pt1.printRecord();
	pt2.printRecord();
	return 0;
}
int main4( void )
{
	Point pt1(10,20);
	Point pt2;
	pt2 = ++ pt1;	//pt2 = pt1.operator++( );
	pt2.printRecord();
	pt1.printRecord();
	return 0;
}
int main3( void )
{
	Point pt1(10,20);
	Point pt2(10,20);
	//bool status = pt1 == pt2;	//bool status = pt1.operator==(pt2);
	//if( status == true  )
	//if( status   )
	/*if( pt1 == pt2 )
		cout<<"Equal"<<endl;
	else
		cout<<"Not Equal"<<endl;*/

	cout<<( pt1 == pt2 ? "Equal" : "Not Equal" )<<endl;
	return 0;
}
int main2( void )
{
	Point pt1(10,20);
	Point pt2 = pt1 + 5;	//pt2 = pt1.operator+( 5 )
	pt2.printRecord();
	return 0;
}

int main1( void )
{
	Point pt1(10,20);
	Point pt2(30,40);
	Point pt3;
	//pt3 = pt1 + pt2;	//pt3 = pt1.operator+( pt2 );
	pt3 = pt1 - pt2;	//pt3 = pt1.operator-( pt2 );
	pt3.printRecord();
	return 0;
}
