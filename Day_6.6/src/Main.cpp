#include<iostream>
using namespace std;
//int *a = &x;
//*a	=>	x


//int *b = &y;
//*b	=>	y
void swap_object( int *a, int *b )
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

void swap_object( int a, int b )
{
	int temp = a;
	a = b;
	b = temp;
}
int main( void )
{
	int x = 10;
	int y = 20;
	//swap_object( x, y );	//Function Call By Value
	swap_object( &x, &y );	//Function Call By Value
	cout<<"X	:	"<<x<<endl;
	cout<<"Y	:	"<<y<<endl;
	return 0;
}
