#include<iostream>
using namespace std;
int main( void )
{
	class Point
	{
	private:
		int xPos;
		int yPos;
	public:
		Point( int xPos = 0, int yPos = 0 )
		{
			this->xPos = xPos;
			this->yPos = yPos;
		}
	};
	Point pt1(10,20);
	Point pt2(30,40);
	Point pt3;
	pt3 = pt1 + pt2;	//OK
	return 0;
}
int main2( void )
{
	struct Point
	{
		int xPos;
		int yPos;
	};
	struct Point pt1 = { 10, 20 };	//Ok
	struct Point pt2 = { 30, 40 };	//Ok
	struct Point pt3;
	//pt3 = pt1 + pt2;	//Not OK
	pt3.xPos = pt1.xPos + pt2.xPos;	//OK
	pt3.yPos = pt1.yPos + pt2.yPos;	//OK

	return 0;
}
int main1( void )
{
	int num1 = 10;	//OK
	int num2 = 20;	//OK
	int result  = num1 + num2;	//OK
	return 0;
}
