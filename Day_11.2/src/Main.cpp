#include<iostream>
#include<string>
using namespace std;

class ArrayIndexOutOfBoundsException
{
private:
	string message;
public:
	ArrayIndexOutOfBoundsException( string message = "ArrayIndexOutOfBoundsException" ) : message( message )
	{	}
	string getMessage( void )const
	{
		return this->message;
	}
};
class Array
{
private:
	int size;
	int *arr;
public:
	Array( void )
	{
		this->size = 0;
		this->arr = nullptr;
	}
	Array( int size )
	{
		this->size = size;
		this->arr = new int[ this->size ];
	}
	int& operator[]( const int index )const
	{
		if( index >= 0 & index < 3 )
			return this->arr[ index ];
		throw ArrayIndexOutOfBoundsException();
	}
	//void resize( int size );
	~Array( void )
	{
		if( this->arr != nullptr )
		{
			delete[] this->arr;
			this->arr = nullptr;
		}
	}
	friend istream& operator>>( istream& cin, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
		{
			cout<<"Enter element	:	";
			cin>>other.arr[ index ];
		}
		return cin;
	}
	friend ostream& operator<<( ostream& cout, Array &other )
	{
		for( int index = 0; index < other.size; ++ index )
			cout<<other.arr[ index ]<<endl;
		return cout;
	}
};
int main( void )
{
	try
	{
		Array a1(3);
		cin>>a1;	//operator>>(cin,a1);

		a1[ 1 ] = 200;	//a1.operator[]( 1 ) = 200;	//20 = 200

		int value = a1[ 1 ];	//int value = a1.operator[]( 1 );
		cout<<"Value	:	"<<value<<endl;

		cout<<a1<<endl;	//operator<<(cout, a1)<<endl;
	}
	catch( ArrayIndexOutOfBoundsException &ex )
	{
		cout<<ex.getMessage()<<endl;
	}
	return 0;
}

int main1( void )
{
	Array a1;
	int value = a1[ 1 ];	//int value = a1.operator[]( 1 );
	cout<<"Value	:	"<<value<<endl;
	return 0;
}
