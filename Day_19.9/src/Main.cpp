#include<iostream>
using namespace std;
class Shape
{
protected:
	float area;
public:
	//Sole Constructor
	Shape( void ) : area( 0 )
	{	}

	virtual void acceptRecord( void ) = 0;

	virtual void calculateArea( void ) = 0;

	void printRecord( void )const
	{
		cout<<"Area	:	"<<this->area<<endl;
	}
	virtual ~Shape(){	}
};
class Rectangle : public Shape
{
private:
	float length;
	float breadth;
public:
	Rectangle( void ) : length( 0 ), breadth( 0 )
	{	}
	void acceptRecord( void )
	{
		cout<<"Length	:	";
		cin>>this->length;
		cout<<"Breadth	:	";
		cin>>this->breadth;
	}
	void calculateArea( void )
	{
		this->area = this->length * this->breadth;
	}
};
class Circle : public Shape
{
private:
	float radius;
public:
	Circle( void ) : radius( 0 )
	{	}
	void acceptRecord( )
	{
		cout<<"Radius	:	";
		cin>>this->radius;
	}
	void calculateArea( void )
	{
		this->area = 3.14f * this->radius * this->radius;
	}
};
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Rectangle"<<endl;
	cout<<"2.Circle"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		Shape *ptr = nullptr;
		switch( choice )
		{
		case 1:
			ptr = new Rectangle();	//Upcasting
			break;
		case 2:
			ptr = new Circle();	//Upcasting
			break;
		}
		if( ptr != nullptr )
		{
			ptr->acceptRecord( );
			ptr->calculateArea( );
			ptr->printRecord();
			delete ptr;
		}
	}
	return 0;
}
