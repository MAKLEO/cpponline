#include<iostream>
#include<string>
using namespace std;

class IllegalArgumentException
{
private:
	string message;
public:
	IllegalArgumentException( string message = "Illegal Argument Exception" ) throw( ) : message( message )
	{	}
	string getMessage( void )const throw( )
	{
		return this->message;
	}
};

class Rectangle
{
private:
	float area;
	float length;
	float breadth;
public:
	Rectangle( void )throw( )
	{	}
	void setLength( const float length )throw( IllegalArgumentException )
	{
		if( length < 0 )
			throw IllegalArgumentException("Invalid length");
		this->length = length;
	}
	void setbreadth( const float breadth )throw( IllegalArgumentException )
	{
		if( breadth < 0 )
			throw IllegalArgumentException("Invalid breadth");
		this->breadth = breadth;
	}
	void calculateArea( void )throw( )
	{
		this->area = this->length * this->breadth;
	}
	float getArea( void )const throw( )
	{
		return this->area;
	}
};

class Math
{
public:
	static const float PI;
public:
	static float pow( float base, int index )
	{
		float result = 1;
		for( int count = 1; count <= index; ++ count )
			result = result * base;
		return result;
	}
};
const float Math::PI = 3.142f;

class Circle
{
private:
	float area;
	float radius;
public:
	Circle( void )throw( )
	{	}
	void setRadius( const float radius )throw( IllegalArgumentException )
	{
		if( radius < 0 )
			throw IllegalArgumentException("Invalid radius");
		this->radius = radius;
	}
	void calculateArea( void )throw( )
	{
		this->area = Math::PI * Math::pow(this->radius, 2);
	}
	float getArea( void )const throw( )
	{
		return this->area;
	}
};
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Rectangle"<<endl;
	cout<<"2.Circle"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		Rectangle *rect = nullptr;
		Circle *c = nullptr;
		switch( choice )
		{
		case 1:
			rect = new Rectangle();
			rect->setLength(10);
			rect->setbreadth(20);
			rect->calculateArea();
			cout<<"Area	:	"<<rect->getArea()<<endl;
			break;
		case 2:
			c = new Circle( );
			c->setRadius(10);
			c->calculateArea();
			cout<<"Area	:	"<<c->getArea()<<endl;
			break;
		}
	}
	return 0;
}
