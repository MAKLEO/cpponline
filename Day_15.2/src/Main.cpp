#include<iostream>
#include<vector>
#include<string>
using namespace std;

class Exception
{
private:
	string message;
public:
	Exception( string message = "" ) : message( message )
	{	}
	string getMessage( void ) const
	{
		return this->message;
	}
};
class Account
{
private:
	string name;
	int number;
	float balance;
public:
	Account( void ) : name( "" ), number( 0 ), balance( 0 )
	{	}
	string getName( void )const
	{
		return this->name;
	}
	void setName( string name )
	{
		this->name = name;
	}
	int getNumber( void )const
	{
		return this->number;
	}
	void setNumber( int number )
	{
		this->number = number;
	}
	float getBalance( void )const
	{
		return this->balance;
	}
	void setBalanace( float balance )
	{
		this->balance = balance;
	}
	friend istream& operator>>( istream &cin, Account &other )
	{
		cout<<"Name	:	";
		cin>>other.name;
		cout<<"Number	:	";
		cin>>other.number;
		cout<<"Balance	:	";
		cin>>other.balance;
		return cin;
	}
	friend ostream& operator<<( ostream &cout, const Account &other )
	{
		cout<<other.name<<"	"<<other.number<<"	"<<other.balance;
		return cout;
	}
};
class Bank
{
private:
	vector<Account> accounts;
public:
	Bank( void )
	{	}
	void createAccount( Account account )
	{
		accounts.push_back(account);
	}
	float deposit( int number, float amount )throw( Exception )
	{
		Account *ptrAccount = exist(number);
		if( accounts.empty( ) )
			throw Exception("Account List is empty");
		if( ptrAccount == nullptr )
			throw Exception("Account not found");
		if( amount < 100  )
			throw Exception("Invalid amount");
		ptrAccount->setBalanace( ptrAccount->getBalance() + amount );
		return ptrAccount->getBalance();
	}
	float withdraw( int number, float amount )throw( Exception )
	{
		Account *ptrAccount = exist(number);
		if( accounts.empty( ) )
			throw Exception("Account List is empty");
		if( ptrAccount == nullptr )
			throw Exception("Account not found");
		if( amount < 100 || ( ptrAccount->getBalance( ) - amount < 0 ) )
			throw Exception("Invalid amount");
		else
			ptrAccount->setBalanace( ptrAccount->getBalance() - amount );
		return ptrAccount->getBalance();
	}
	void printAccountDetails( int number )throw( Exception  )
	{
		if( accounts.empty( ) )
			throw Exception("Account List is empty");
		if( this->exist(number) == nullptr )
			throw Exception("Account not found");
		for( vector<Account>::iterator itr = accounts.begin(); itr != accounts.end(); ++ itr )
		{
			if( (*itr).getNumber() == number )
			{
				cout<< (*itr)<<endl;
				break;
			}
		}
	}
	void printAccounts( void )throw( Exception  )
	{
		if( accounts.empty( ) )
			throw Exception("Account List is empty");
		/*for( vector<Account>::iterator itr = accounts.begin(); itr != accounts.end(); ++ itr )
			cout<<(*itr)<<endl;*/

		for( Account acc : this->accounts )
			cout<<acc<<endl;
	}
private:
	Account* exist( int number )
	{
		for( vector<Account>::iterator itr = accounts.begin(); itr != accounts.end(); ++ itr )
		{
			if( (*itr).getNumber() == number )
				return &(*itr);
		}
		return nullptr;
	}
};
int main( void )
{
	return 0;
}
