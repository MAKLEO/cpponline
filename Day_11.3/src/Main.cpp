#include<iostream>
using namespace std;
class Point
{
private:
	int xPos;
	int yPos;
public:
	Point( void )
	{
		cout<<"Point( void )"<<endl;
		this->xPos = 0;
		this->yPos = 0;
	}
	Point( int xPos, int yPos )
	{
		cout<<"Point( int xPos, int yPos )"<<endl;
		this->xPos = xPos;
		this->yPos = yPos;
	}
	//const Point &other = pt1
	//Point *const this = &pt2;
	Point( const Point &other )
	{
		cout<<"Point( const Point &other )"<<endl;
		this->xPos = other.xPos;
		this->yPos = other.yPos;
	}
	//const Point &other = pt1
	//Point *const this = &pt2;
	Point& operator=( const Point &other )
	{
		cout<<"void operator=( const Point &other )"<<endl;
		this->xPos = other.xPos;
		this->yPos = other.yPos;
		return (*this);
	}
	friend ostream& operator<<( ostream &cout, Point &other )
	{
		cout<<"X Position	:	"<<other.xPos<<endl;
		cout<<"Y Position	:	"<<other.yPos<<endl;
		return cout;
	}
};
int main( void )
{
	Point pt1(10,20);
	Point pt2;
	Point pt3;

	pt3 = pt2 = pt1;	//pt3.operator=(pt2.operator=(pt1))

	cout<<pt3;
	return 0;
}
int main1( void )
{
	Point pt1(10,20);
	Point pt2;
	pt2 = pt1;	//pt2.operator=( pt1 )
	cout<<pt2;	//operator<<( cout, pt2 );
	return 0;
}
