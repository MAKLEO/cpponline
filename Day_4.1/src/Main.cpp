#include<iostream>
#include<string>
using namespace std;

namespace nemployee
{
	class Employee
	{
	private:
		string name;
		int empid;
		float salary;
	public:
		//Employee *const this = &emp;
		void acceptRecord( void )
		{
			cout<<"Name	:	";
			cin>>this->name;
			cout<<"Empid	:	";
			cin>>this->empid;
			cout<<"Salary	:	";
			cin>>this->salary;
		}
		//Employee *const this = &emp
		void printRecord( void )
		{
			cout<<"Name	:	"<<this->name<<endl;
			cout<<"Empid	:	"<<this->empid<<endl;
			cout<<"Salary	:	"<<this->salary<<endl;
		}
	};
}
int main( void )
{
	using namespace nemployee;
	Employee emp;
	emp.acceptRecord();	//emp.acceptRecord( &emp );
	emp.printRecord();	//emp.printRecord(&emp);
	return 0;
}
