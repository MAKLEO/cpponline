#ifndef ARITHMETICEXCEPTION_H_
#define ARITHMETICEXCEPTION_H_

#include<string>
using namespace std;

namespace cpponline
{
	class ArithmeticException
	{
	private:
		string message;
	public:
		ArithmeticException( string message = "ArithmeticException" );
		string getMessage( void )const;
	};
}
#endif /* ARITHMETICEXCEPTION_H_ */
