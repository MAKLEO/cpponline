#include"../include/ArithmeticException.h"
using namespace cpponline;

int calculate(int num1, int num2 )throw( ArithmeticException )
{
	if( num2 == 0 )
		throw ArithmeticException("/ by 0");
	return num1 / num2;
}
