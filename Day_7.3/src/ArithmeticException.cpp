#include"../include/ArithmeticException.h"
using namespace cpponline;
ArithmeticException::ArithmeticException( string message ) : message( message )
{ }
string ArithmeticException::getMessage( void )const
{
	return this->message;
}
