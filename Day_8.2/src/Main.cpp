#include<iostream>
using namespace std;

class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void )
	{
		cout<<"Complex( void )"<<endl;
		this->real = 0;
		this->imag = 0;
	}
	Complex( int real, int imag )
	{
		cout<<"Complex( int real, int imag )"<<endl;
		this->real = real;
		this->imag = imag;
	}
	void acceptRecord( void )
	{
		cout<<"Enter real number	:	";
		cin>>this->real;
		cout<<"Enter imag number	:	";
		cin>>this->imag;
	}
	void printRecord( void )
	{
		cout<<this->real<<"	"<<this->imag<<endl;
	}
};
int main( void )
{
	Complex arr[ 3 ] = { Complex(10,20 ), Complex(30,40), Complex(50,60)};
	for( int index = 0; index < 3; ++ index )
		arr[ index ].printRecord();
	return 0;
}
int main6( void )
{
	Complex *arr = new Complex[ 3 ];
	for( int index = 0; index < 3; ++ index )
		arr[ index ].acceptRecord();
	for( int index = 0; index < 3; ++ index )
		arr[ index ].printRecord();
	delete[] arr;
	arr = nullptr;
	return 0;
}
int main5( void )
{
	Complex arr[ 3 ];
	for( int index = 0; index < 3; ++ index )
		arr[ index ].acceptRecord();
	for( int index = 0; index < 3; ++ index )
		arr[ index ].printRecord();
	return 0;
}
int main4( void )
{
	Complex *ptr = new Complex( 10, 20 );	//Complex( int real, int imag )
	ptr->printRecord();//10	20
	delete ptr;
	return 0;
}

int main3( void )
{
	Complex c1(10, 20 );	//Complex( int real, int imag )
	c1.printRecord();	//10	20
	return 0;
}

int main2( void )
{
	//Complex *ptr = new Complex;	//Complex( void )
	Complex *ptr = new Complex( );	//Complex( void )
	ptr->printRecord();//0	0
	delete ptr;
	return 0;
}

int main1( void )
{
	Complex c1;	//Complex( void )
	c1.printRecord();//0	0
	return 0;
}
