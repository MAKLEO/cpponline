#include<iostream>
using namespace std;
#include "../include/Rectangle.h"
#include "../include/Circle.h"
#include"../include/ShapeFactory.h"
using namespace cpponline;

void accept_record( Shape *ptr )
{
	if( dynamic_cast<Rectangle*>( ptr ) != nullptr )
	{
		Rectangle *ptrRect = dynamic_cast<Rectangle*>( ptr );
		float length;
		cout<<"Length	:	";
		cin>>length;
		ptrRect->setLength(length);

		float breadth;
		cout<<"Breadth	:	";
		cin>>breadth;
		ptrRect->setbreadth(breadth);
	}
	else
	{
		Circle *ptrCircle = dynamic_cast<Circle*>( ptr );
		float radius;
		cout<<"Radius	:	";
		cin>>radius;
		ptrCircle->setRadius(radius);
	}
}
void print_record( Shape *ptr )
{
	cout<<"Area of instance of "<<typeid( *ptr ).name()<<" is : "<<ptr->getArea()<<endl;
}
int menu_list( void )
{
	int choice;
	cout<<"0.Exit"<<endl;
	cout<<"1.Rectangle"<<endl;
	cout<<"2.Circle"<<endl;
	cout<<"Enter choice	:	";
	cin>>choice;
	return choice;
}
int main( void )
{
	int choice;
	while( ( choice = ::menu_list( ) ) != 0 )
	{
		try
		{
			Shape *ptr = ShapeFactory::getObject(choice) ;
			if( ptr != nullptr )
			{
				try
				{
					::accept_record( ptr );
					ptr->calculateArea();
					::print_record( ptr );
				}
				catch( IllegalArgumentException &ex )
				{
					cout<<ex.getMessage()<<endl;
				}
			}
		}
		catch( bad_alloc &ex )
		{
			cout<<ex.what()<<endl;
		}
	}
	return 0;
}
