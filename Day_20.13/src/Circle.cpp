#include"../include/Circle.h"
#include"../include/Math.h"
using namespace cpponline;
Circle::Circle( void )throw( ) : radius( 0 )
{	}
void Circle::setRadius( const float radius )throw( IllegalArgumentException )
{
	if( radius < 0 )
		throw IllegalArgumentException("Invalid radius");
	this->radius = radius;
}
void Circle::calculateArea( void )throw( )
{
	this->area = Math::PI * Math::pow(this->radius, 2);
}

