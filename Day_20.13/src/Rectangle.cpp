#include"../include/Rectangle.h"
using namespace cpponline;
Rectangle::Rectangle( void )throw( ) : length( 0 ), breadth( 0 )
{	}
void Rectangle::setLength( const float length )throw( IllegalArgumentException )
{
	if( length < 0 )
		throw IllegalArgumentException("Invalid length");
	this->length = length;
}
void Rectangle::setbreadth( const float breadth )throw( IllegalArgumentException )
{
	if( breadth < 0 )
		throw IllegalArgumentException("Invalid breadth");
	this->breadth = breadth;
}
void Rectangle::calculateArea( void )throw( )
{
	this->area = this->length * this->breadth;
}
