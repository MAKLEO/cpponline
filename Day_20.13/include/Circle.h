#ifndef CIRCLE_H_
#define CIRCLE_H_
#include"../include/Shape.h"
#include"../include/IllegalArgumentException.h"
using namespace cpponline;

namespace cpponline
{
	class Circle : public Shape
	{
	private:
		float radius;
	public:
		Circle( void )throw( );
		void setRadius( const float radius )throw( IllegalArgumentException );
		void calculateArea( void )throw( );
	};
}
#endif /* CIRCLE_H_ */
