#ifndef SHAPE_H_
#define SHAPE_H_
namespace cpponline
{
	class Shape
	{
	protected:
		float area;
	public:
		Shape( void )throw( );

		virtual void calculateArea( void )throw( ) = 0;

		float getArea( void )const throw( );

		virtual ~Shape( );
	};
}
#endif /* SHAPE_H_ */
