################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
../src/Circle.cpp \
../src/IllegalArgumentException.cpp \
../src/Main.cpp \
../src/Math.cpp \
../src/Rectangle.cpp \
../src/Shape.cpp \
../src/ShapeFactory.cpp 

OBJS += \
./src/Circle.o \
./src/IllegalArgumentException.o \
./src/Main.o \
./src/Math.o \
./src/Rectangle.o \
./src/Shape.o \
./src/ShapeFactory.o 

CPP_DEPS += \
./src/Circle.d \
./src/IllegalArgumentException.d \
./src/Main.d \
./src/Math.d \
./src/Rectangle.d \
./src/Shape.d \
./src/ShapeFactory.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: GCC C++ Compiler'
	g++ -O0 -g3 -Wall -c -fmessage-length=0 -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


