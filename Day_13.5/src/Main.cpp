#include<iostream>
using namespace std;

class Test
{
private:
	int num1;	//Instance Variable
	int num2;	//Instance Variable
	int num3;	//Instance Variable
public:
	Test( int num1 = 0, int num2 = 0, int num3 = 0 )
	{
		this->num1 = num1;
		this->num2 = num2;
		this->num3 = num3;
	}
	void printRecord( void )const
	{
		cout<<"Num1	:	"<<this->num1<<endl;
		cout<<"Num2	:	"<<this->num2<<endl;
		cout<<"Num3	:	"<<this->num3<<endl;
		cout<<endl;
	}
};
int main( void )
{
	Test t1(10,20,500);
	//cout<<"Size	:	"<<sizeof(t1)<<endl;
	Test t2(30,40,500);
	//cout<<"Size	:	"<<sizeof(t2)<<endl;
	Test t3(50,60,500);
	//cout<<"Size	:	"<<sizeof(t3)<<endl;

	t1.printRecord();
	t2.printRecord();
	t3.printRecord();
	return 0;
}
