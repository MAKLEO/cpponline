#include<iostream>
using namespace std;
namespace naccount
{
	class Account
	{
	private:
		char name[ 30 ];
		int number;
		float balance;
	public:
		void acceptRecord( void )
		{
			cout<<"Name	:	";
			cin>> name;
			cout<<"Number	:	";
			cin>>number;
			cout<<"Balance	:	";
			cin>>balance;
		}
		void printRecord( void )
		{
			cout<<"Name	:	"<< name<<endl;
			cout<<"Number	:	"<< number<<endl;
			cout<<"Balance	:	"<< balance<<endl;
		}
	};//end of class
}//end of namespace
int main( void )
{
	//naccount::Account acc;
	using namespace naccount;
	Account acc;
	acc.acceptRecord( );
	acc.printRecord( );
	return 0;
}
