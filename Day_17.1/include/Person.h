#ifndef PERSON_H_
#define PERSON_H_

#include<string>
using namespace std;

namespace cpponline
{
	class Person
	{
	protected:
		string name;
		int age;
	public:
		Person( void );
		Person( string name, int age );
		void showRecord( void );
	};
}

#endif /* PERSON_H_ */
