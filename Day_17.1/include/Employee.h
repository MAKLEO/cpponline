#ifndef EMPLOYEE_H_
#define EMPLOYEE_H_

#include"../include/Person.h"
using namespace cpponline;

namespace cpponline
{
	class Employee : public Person	//Employee is a Person
	{
	private:
		int empid;
		float salary;
	public:
		Employee( void );
		Employee( string name, int age, int empid, float salary );
		void displayRecord( void );
	};
}
#endif /* EMPLOYEE_H_ */
