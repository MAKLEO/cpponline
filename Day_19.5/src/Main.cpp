#include<iostream>
using namespace std;
class A
{
private:
	//v-ptr
	int num1;
	int num2;
public:
	A( void )
	{
		this->num1 = 10;
		this->num2 = 20;
	}
	virtual void f1( void )
	{	}
	virtual void f2( void )
	{	}
	void f3( void )
	{	}
};
class B : public A
{
private:
	int num3;
public:
	B( void )
	{
		this->num3 = 30;
	}
	virtual void f1( void )
	{	}
	virtual void f4( void )
	{	}
};
int main( void )
{
	A *ptr = new A( );
	return 0;
}
