#include<iostream>
#include<fstream>
using namespace std;
class Complex
{
private:
	int real;
	int imag;
public:
	Complex( void ) : real( 10 ), imag( 20 )
	{	}
	friend ostream& operator<<( ostream &cout, const Complex &other )
	{
		cout<<"Real Number	:	"<<other.real<<endl;
		cout<<"Image Number	:	"<<other.imag<<endl;
		return cout;
	}
	friend int main( void );
};
int main( void )
{
	Complex c1;
	cout<<c1<<endl;	//operator<<( cout, c1 )<<endl;

	Complex *ptrComplex = &c1;
	//int *ptrInt = (int*)ptrComplex;
	int *ptrInt = reinterpret_cast<int*>( ptrComplex );
	*ptrInt = 50;
	ptrInt = ptrInt + 1;
	*ptrInt = 60;

	cout<<c1<<endl;	//operator<<( cout, c1 )<<endl;
	return 0;
}
int main1( void )
{
	int number = 10;
	int *ptrNumber = &number;
	//cout<<"Number	:	"<<number<<endl;	//10
	cout<<"Number	:	"<<*ptrNumber<<endl;	//10	<= Dereferencing
	return 0;
}
